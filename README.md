```
 ____  _                 __   ____
| __ )| |_   _  ___     / /  / ___|_ __ ___  ___ _ __
|  _ \| | | | |/ _ \   / /  | |  _| '__/ _ \/ _ \ '_ \
| |_) | | |_| |  __/  / /   | |_| | | |  __/  __/ | | |
|____/|_|\__,_|\___| /_/     \____|_|  \___|\___|_| |_|

 ____             _                        _ _   _
|  _ \  ___ _ __ | | ___  _   _  __      _(_) |_| |__
| | | |/ _ \ '_ \| |/ _ \| | | | \ \ /\ / / | __| '_ \
| |_| |  __/ |_) | | (_) | |_| |  \ V  V /| | |_| | | |
|____/ \___| .__/|_|\___/ \__, |   \_/\_/ |_|\__|_| |_|
           |_|            |___/
 _   _      _
| | | | ___| |_ __ ___
| |_| |/ _ \ | '_ ` _ \
|  _  |  __/ | | | | | |
|_| |_|\___|_|_| |_| |_|
```
# Workflow
![Blue/Green](helm/images/blue_green.png)

# Prepare minikube
```bash
$ minikube start --driver=hyperkit --kubernetes-version=latest --cpus 4 --memory 8192
```
# Setup demo.local hostname in localhost
```
# DEMO_IP=`minikube ip`
# sudo echo "$DEMO_IP demo.local" >> /etc/hosts
```

## setup release cadidate with tag
```
v[0-9].[0-9]-rc[0-9]
```
## setup release with tag
```
v[0-9].[0-9]
```
## checkout the reference

[Gitlab Pipelines](https://gitlab.com/maohsiang_lien/blue-green/-/pipelines)

[Dockerhub image](https://hub.docker.com/repository/docker/titanlien/web)
